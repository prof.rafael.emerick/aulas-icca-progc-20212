#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#define TAM 30
#define KEY 180

void clearScreen()
{
      const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J#> ";
      write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);    
}

void criptoSin(char *comando, int chave, char *criptoMsg)
{
    int i;
    for(i=0; i<TAM ;i++)
        criptoMsg[i] = comando[i]*sin(chave);
    

}

void decodeCmd(char *comando)
{
    if (strcmp(comando,"clear") == 0)  // compara o comando com "clear"
        clearScreen();
    else
        printf("%s\n#>", comando);
  
}


int main()
{
    char cmd[TAM],criptoMsg[TAM];
    int chave=KEY;
    clearScreen();
    while(1)
    {
        scanf("%s",cmd);
        decodeCmd(cmd);
        criptoSin(cmd,chave,criptoMsg);
        printf("%s", criptoMsg);
    }
}