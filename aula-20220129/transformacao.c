#include <stdio.h>
#include <math.h>

#define NOME_ARQ_ENTRADA "entrada.dat"
#define NOME_ARQ_SAIDA "saida.dat"
#define LINHA 7
#define COLUNA 25

double converte(int angulo)
{
    return angulo * M_PI / 180; 
}



int main()
{
    FILE *file_in,*file_out;
    
    file_in = fopen(NOME_ARQ_ENTRADA,"r");
    file_out = fopen(NOME_ARQ_SAIDA,"w");

    double x,y;
    char linha, cores[LINHA][COLUNA], coisas[LINHA][COLUNA];
    int count, modulos[LINHA], angulos[LINHA];
    

    //tratar a condição de erro na abertura do arquivo
    if( file_in == NULL || file_out == NULL )
    {
        printf("\n Arquivos %s ou %s não puderam ser abertos \n\n", NOME_ARQ_ENTRADA, NOME_ARQ_SAIDA);
        return -1;
    }
    else
    {
        count=0;
        linha = fscanf(file_in, "%s %s %d %d",cores[count],coisas[count],&modulos[count],&angulos[count]);
        while( linha != EOF ) //EOF é uma caracter que sinaliza o final do arquivo
        {
         
            x = modulos[count] * cos(converte(angulos[count]));
            y = modulos[count] * sin(converte(angulos[count]));

            //printf("%s %s %d %d %f %f\n", cores[count],coisas[count],modulos[count],angulos[count],x,y);
            fprintf(file_out,"%s %s %d %d %f %f\n", cores[count],coisas[count],modulos[count],angulos[count],x,y);
            count++;
            linha = fscanf(file_in, "%s %s %d %d",cores[count],coisas[count],&modulos[count],&angulos[count]);

        }
        fclose(file_in);
        fclose(file_out);
    }

    
}