#include <stdio.h>
#include <math.h>
#include <string.h>

#define NOME_ARQ_ENTRADA "entrada.dat"
#define NOME_ARQ_SAIDA "saida.dat"
#define LINHA 7
#define COLUNA 25

double converte(int angulo)
{
    return angulo * M_PI / 180; 
}

int acha_maior(int *tam_palavras)
{
    int maior, i,posicao;

    posicao=0;
    maior = tam_palavras[posicao];
    for(i=0; i< LINHA; i++)
    {
        if(tam_palavras[i]>maior)
        {
            maior = tam_palavras[i];
            posicao = i;
        }    

    }
    return posicao;

}



int main()
{
    FILE *file_in,*file_out;
    
    file_in = fopen(NOME_ARQ_ENTRADA,"r");
    file_out = fopen(NOME_ARQ_SAIDA,"w");

    double x,y;
    char linha, cores[LINHA][COLUNA], coisas[LINHA][COLUNA];
    int count, modulos[LINHA], angulos[LINHA], tamanho_palavras[LINHA];
    

    //tratar a condição de erro na abertura do arquivo
    if( file_in == NULL || file_out == NULL )
    {
        printf("\n Arquivos %s ou %s não puderam ser abertos \n\n", NOME_ARQ_ENTRADA, NOME_ARQ_SAIDA);
        return -1;
    }
    else
    {
        count=0;
        linha = fscanf(file_in, "%s %s %d %d",cores[count],coisas[count],&modulos[count],&angulos[count]);
        while( linha != EOF ) //EOF é uma caracter que sinaliza o final do arquivo
        {
            // converte de angular para cartesiano
            x = modulos[count] * cos(converte(angulos[count]));
            y = modulos[count] * sin(converte(angulos[count]));

            //soma a quantidade de bytes da posicao count de cores e coisas
            tamanho_palavras[count] = strlen(cores[count]) + strlen(coisas[count]);
            
            //printf("%s %s %d %d %f %f\n", cores[count],coisas[count],modulos[count],angulos[count],x,y);
            fprintf(file_out,"%s %s %d %d %f %f %d\n", cores[count],coisas[count],modulos[count],angulos[count],x,y, tamanho_palavras[count]);
            count++;
            linha = fscanf(file_in, "%s %s %d %d",cores[count],coisas[count],&modulos[count],&angulos[count]);

        }
        printf("a maior palavra esta na posicao %d\n", acha_maior(tamanho_palavras));

        fclose(file_in);
        fclose(file_out);
    }

    
}